/**
 * Created by Kevin on 5/22/2015.
 */

var gulp = require("gulp");
var less = require("gulp-less");
var sourcemaps = require('gulp-sourcemaps');
var prefix = require('gulp-autoprefixer');
var path = require('path');

gulp.task('default', function() {
	var message = "Available tasks: \n"+
	"less		Compiles all *.less files in the public/stylesheets/less directory. Omits files starting with an underscore (_).\n"+
	"less-watch	Watches all *.less files in the public/stylesheets/less directory and compiles them when changes are detected." ;
    console.log(message);
});

var lessFiles = "public/stylesheets/less/**/[^_]*.less";

gulp.task('less', function() {
    return gulp.src(lessFiles)
        .pipe(sourcemaps.init())
        .pipe(less().on('error', function(e) {
            console.log(e.message);
            this.emit('end');
            //http://stackoverflow.com/questions/28166409/gulp-js-stops-compiling-less-when-watched-after-there-has-been-an-error-in-the-l
        }))
        .pipe(prefix())
        .pipe(sourcemaps.write(".", {includeContent: false, sourceRoot: '/assets/stylesheets/less'}))
        .pipe(gulp.dest('public/stylesheets'));
});

gulp.task('less-watch', ['less'], function() {
    return gulp.watch("public/stylesheets/less/**/*.less", ['less']);
});